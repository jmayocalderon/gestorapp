// Iniciamos paquetes necesarios para crear app Express
const express = require('express');
const cors = require('cors');
const helmet = require('helmet');

// Iniciar App Express
let app = express();

// Definir puerto por el que se escuchara
const PORT = 3000;

// Configurar seguridad por defecto a App Express
app.use(cors());
app.use(helmet());

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/api', require('./rutas/general.ruta'));

// Levantar el proyecto
app.listen(PORT, () => {
  console.log(`Servidor HTTP corriendo en: http://localhost:${PORT}`);
});
