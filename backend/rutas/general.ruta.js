const express = require('express');
const api = express.Router();

const Usuarios = require('../controladores/usuario.controlador');

api.get('/usuarios', Usuarios.obtenerTodos);
api.get('/usuario/:ncodigo', Usuarios.obtenerUno);
api.post('/usuario', Usuarios.crear);
api.delete('/usuario/:ncodigo', Usuarios.eliminar);

module.exports = api;