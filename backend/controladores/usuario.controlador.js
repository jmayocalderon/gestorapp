const { Sequelize, QueryTypes } = require('sequelize');

ListaUsuarios = () => new Promise(async(resolve, rejected) => {
  const sequelize = new Sequelize('gestordb', 'postgres', '123456', {
    host: 'localhost',
    dialect: 'postgres',
    port: '5432'
  });

  try {
    await sequelize.authenticate();
    let sql = "select * from usuarios";
    let parametros = {};
    let tipo = QueryTypes.SELECT;
    let resultado = await sequelize.query(sql, {replacements: parametros}, tipo);
    resolve(resultado[0]);
  } catch (error) {
    rejected(error);
  }
});

NuevoUsuario = (nuevo) => new Promise(async(resolve, rejected) => {
  const sequelize = new Sequelize('gestordb', 'postgres', '123456', {
    host: 'localhost',
    dialect: 'postgres',
    port: '5432'
  });

  try {
    await sequelize.authenticate();
    let sql = "select * from fx_gen_insusu(:nuevo)";
    let parametros = {
      nuevo: JSON.stringify(nuevo)
    };
    let tipo = QueryTypes.SELECT;
    let resultado = await sequelize.query(sql, {replacements: parametros}, tipo);
    resolve(resultado[0]);
  } catch (error) {
    rejected(error);
  }
});

EliminarUsuario = (ncodigo) => new Promise(async(resolve, rejected) => {
  const sequelize = new Sequelize('gestordb', 'postgres', '123456', {
    host: 'localhost',
    dialect: 'postgres',
    port: '5432'
  });

  try {
    await sequelize.authenticate();
    let sql = "select * from fx_gen_delusu(:ncodigo)";
    let parametros = {
      ncodigo: ncodigo
    };
    let tipo = QueryTypes.SELECT;
    let resultado = await sequelize.query(sql, {replacements: parametros}, tipo);
    if (resultado.length) {
      resolve({elimino: resultado[0][0].fx_gen_delusu});
    } else {
      resolve({elimino: false});
    }
  } catch (error) {
    rejected(error);
  }
});

UnUsuario = (ncodigo) => new Promise(async(resolve, rejected) => {
  const sequelize = new Sequelize('gestordb', 'postgres', '123456', {
    host: 'localhost',
    dialect: 'postgres',
    port: '5432'
  });

  try {
    await sequelize.authenticate();
    let sql = "select * from fx_gen_selusu(:ncodigo)";
    let parametros = {
      ncodigo: ncodigo
    };
    let tipo = QueryTypes.SELECT;
    let resultado = await sequelize.query(sql, {replacements: parametros}, tipo);
    if (resultado.length) {
      resolve(resultado[0][0]);
    } else {
      resolve(null);
    }
  } catch (error) {
    rejected(error);
  }
});

obtenerTodos = async (req, res) => {
  try {
    let resultado = await ListaUsuarios();
    res.status(200).json(resultado);
  } catch (error) {
    res.status(500).json(error);
  }
}

crear = async (req, res) => {
  try {
    let resultado = await NuevoUsuario(req.body.nuevo);
    res.status(200).json(resultado);
  } catch (error) {
    res.status(500).json(error);
  }
}

eliminar = async (req, res) => {
  try {
    let resultado = await EliminarUsuario(req.params.ncodigo);
    res.status(200).json(resultado);
  } catch (error) {
    console.log(error);
  }
}

obtenerUno = async (req, res) => {
  try {
    let resultado = await UnUsuario(req.params.ncodigo);
    res.status(200).json(resultado);
  } catch (error) {
    res.status(500).json(error);
  }
}

module.exports = {
  obtenerTodos,
  crear,
  eliminar,
  obtenerUno,
  ListaUsuarios
}